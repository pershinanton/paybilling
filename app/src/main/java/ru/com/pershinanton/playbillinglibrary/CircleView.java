package ru.com.pershinanton.playbillinglibrary;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

public class CircleView extends View {
    private static final int DEFAULT_CIRCLE_COLOR = Color.RED;

    public boolean checked;
    private int circleColor = DEFAULT_CIRCLE_COLOR;
    private Paint paintChecked;
    private Paint paintUnChecked;


    public CircleView(Context context) {
        super(context);
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void init(int w, int h) {
        if (paintChecked == null || paintUnChecked == null) {
            paintUnChecked = new Paint();
            paintUnChecked.setAntiAlias(true);
            paintUnChecked.setColor(circleColor);

            paintChecked = new Paint();
            paintChecked.setStrokeWidth(1);
            paintChecked.setStyle(Paint.Style.FILL_AND_STROKE);
            paintChecked.setColor(circleColor);
            paintChecked.setShader(new RadialGradient(w / 2, h / 2,
                    h / 0.5f, Color.TRANSPARENT, getCircleColor()/*XML_PARAM*/, Shader.TileMode.CLAMP));
        }
    }

//     создать параметр который getCircleColor указали в хмл

    public int getCircleColor() {
        return circleColor;
    }

    public void setCircleColor(int circleColor) {
        this.circleColor = circleColor;
        if (paintChecked != null) paintChecked.setColor(circleColor);
        if (paintUnChecked != null) paintUnChecked.setColor(circleColor);
        invalidate();
    }


    public void setChecked(boolean checked) {
        this.checked = checked;
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (canvas.getWidth() == 0 || canvas.getHeight() == 0) return;
        init(canvas.getWidth(), canvas.getHeight());

        if (checked) {
            canvas.drawCircle(canvas.getHeight() / 2, canvas.getHeight() / 2, canvas.getHeight() / 2, paintChecked);
            canvas.drawCircle(canvas.getHeight() / 2, canvas.getHeight() / 2, canvas.getHeight() / 2.7f, paintUnChecked);
        } else {
            canvas.drawCircle(canvas.getHeight() / 2, canvas.getHeight() / 2, canvas.getHeight() / 2.7f, paintUnChecked);
        }

    }

}