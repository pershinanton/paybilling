package ru.com.pershinanton.playbillinglibrary;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ColorsAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private final ItemOffsetDecoration itemOffsetDecoration = new ItemOffsetDecoration();

    private final List<Integer> colors = new ArrayList<>();
    private RecyclerView recyclerView;

    private ItemViewHolder.Listener listener;

    public ColorsAdapter(RecyclerView recyclerView, ItemViewHolder.Listener listener) {
        this.recyclerView = recyclerView;
        this.listener = listener;
        recyclerView.addItemDecoration(itemOffsetDecoration);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(parent.getContext(), listener);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        ItemViewHolder vh = (ItemViewHolder) holder;
        vh.onBind(position, colors.get(position));
    }

    @Override
    public void onViewRecycled(@NonNull BaseViewHolder holder) {
        super.onViewRecycled(holder);
        if(holder instanceof BaseViewHolder) {
            BaseViewHolder vh = holder;
            vh.onViewRecycled();
        }
    }

    @Override
    public int getItemCount() {
        return colors.size();
    }

    public void addItem(Integer model) {
        colors.add(model);
        notifyDataSetChanged();
    }

    public void setModels(List<Integer> models) {
        this.colors.clear();
        if (models != null)
            this.colors.addAll(models);
        notifyDataSetChanged();
    }

    public void release() {
        if(recyclerView != null) {
            recyclerView.removeItemDecoration(itemOffsetDecoration);
            recyclerView = null;
        }
        colors.clear();
        notifyDataSetChanged();
    }

    public static class ItemViewHolder extends BaseViewHolder<Integer> {

        private final Listener listener;

        @BindView(R.id.circleView)
        CircleView circleView;

        ItemViewHolder(Context context, Listener listener) {
            super(context, R.layout.item_list);
            this.listener = listener;
        }

        @Override
        public void onBind(int position, Integer color) {
            super.onBind(position, color);
            circleView.setCircleColor(color);
            if(listener!=null)
                circleView.setChecked(listener.isChecked(color));
        }

        @OnClick(R.id.circleView)
        void circleView() {
            if(listener!=null) listener.onClickItem(getBindObject());
        }

        public interface Listener {
            void onClickItem(int color);
            boolean isChecked(int color);
        }

    }

    protected class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

        public ItemOffsetDecoration() {
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            long position = parent.getChildAdapterPosition(view);
            if (position >= 0) {
                if (position == 0) {
                    outRect.left = (int) parent.getContext().getResources().getDimension(R.dimen.default_margin);
                    outRect.right = (int) parent.getContext().getResources().getDimension(R.dimen.half_margin);
                } else if (position == getItemCount() - 1) {
                    outRect.left = (int) parent.getContext().getResources().getDimension(R.dimen.half_margin);
                    outRect.right = (int) parent.getContext().getResources().getDimension(R.dimen.default_margin);
                } else {
                    outRect.left = (int) parent.getContext().getResources().getDimension(R.dimen.half_margin);
                    outRect.right = (int) parent.getContext().getResources().getDimension(R.dimen.half_margin);
                }
            } else {
                outRect.left = 0;
                outRect.right = 0;
            }
        }
    }

}
