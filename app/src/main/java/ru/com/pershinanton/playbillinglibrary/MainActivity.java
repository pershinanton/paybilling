package ru.com.pershinanton.playbillinglibrary;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.widget.Toast.makeText;

public class MainActivity extends AppCompatActivity implements PurchasesUpdatedListener {


    ColorsAdapter adapter;
    private final String TAG = "myBillingClient";

    private BillingClient mBillingClient;
    private Set<String> tokensToBeConsumed;
    private Map<String, SkuDetails> mSkuDetailsMap = new HashMap<>();
    private final List<Purchase> myPurchasesResultList = new ArrayList<>();
    private String mSkuId1 = "bylka";
    private int minSize = 38;
    private int maxSize = 42;
    private int currentSize = 38;
    @BindView(R.id.mRecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.sizeText)
    TextView sizeText;
    @BindView(R.id.image)
    ImageView imageView;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.radio1)
    RadioButton radio1;
    @BindView(R.id.radio2)
    RadioButton radio2;
    @BindView(R.id.radio3)
    RadioButton radio3;
    @BindView(R.id.radio4)
    RadioButton radio4;

    private Unbinder unbinder;
    private Integer color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        initBilling();
        initButterKnife();
        initRecyclerView();

    }

    @OnClick(R.id.buttonPay)
    void buttonPay() {
        launchBilling(mSkuId1);
    }

    @OnClick(R.id.sizeDown)
    void sizeDown() {
        if (currentSize > maxSize || currentSize != minSize)
            currentSize--;
        sizeText.setText(String.valueOf(currentSize));
    }

    @OnClick(R.id.sizeUp)
    void sizeUp() {
        if (currentSize < maxSize || currentSize != maxSize)
            currentSize++;
        sizeText.setText(String.valueOf(currentSize));
    }

    @OnCheckedChanged({R.id.radio1, R.id.radio2, R.id.radio3, R.id.radio4})
    public void onRadioButtonCheckChanged(CompoundButton button, boolean checked) {
        if (checked) {
            switch (button.getId()) {
                case R.id.radio1:
                    imageView.setBackgroundColor(Color.YELLOW);
                    break;
                case R.id.radio2:
                    imageView.setBackgroundColor(Color.BLUE);
                    break;
                case R.id.radio3:
                    imageView.setBackgroundColor(Color.RED);
                    break;
                case R.id.radio4:
                    imageView.setBackgroundColor(Color.GREEN);
                    break;
            }
        }
    }


    public void initButterKnife() {
        unbinder = ButterKnife.bind(this);
    }


    private void initRecyclerView() {
        new StartSnapHelper().attachToRecyclerView(recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ColorsAdapter(recyclerView, new ColorsAdapter.ItemViewHolder.Listener() {

            @Override
            public void onClickItem(int color) {
                MainActivity.this.color = color;
                adapter.notifyDataSetChanged();
            }

            @Override
            public boolean isChecked(int color) {
                return MainActivity.this.color != null && MainActivity.this.color == color;
            }
        });

        List<Integer> models = new ArrayList();
        models.add(Color.BLUE);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.BLUE);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.BLUE);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.BLUE);
        models.add(Color.RED);
        models.add(Color.BLUE);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.BLUE);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.BLUE);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.BLUE);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.BLUE);
        models.add(Color.RED);
        models.add(Color.BLUE);
        models.add(Color.RED);
        models.add(Color.RED);
        models.add(Color.RED);
        adapter.setModels(models);

        recyclerView.setAdapter(adapter);
    }

    private void initBilling() {
        mBillingClient = BillingClient.newBuilder(this)
                .setListener(this)
                .enablePendingPurchases()
                .build();
        mBillingClient.startConnection(new BillingClientStateListener() {


            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    Log.d(TAG, "purchase");
                    //здесь мы можем запросить информацию о товарах и покупках
                    querySkuDetailsSubs();
                    queryPurchasesAsync();
//                    querySkuDetailsInApp();
                    List<Purchase> purchasesList = queryPurchases(); //запрос о покупках

                    //если товар уже куплен, предоставить его пользователю
                    for (int i = 0; i < purchasesList.size(); i++) {
                        String purchaseId = purchasesList.get(i).getSku();
                        if (TextUtils.equals(mSkuId1, purchaseId)) {
                            payComplete();
                        }
                    }
                }
            }

            private List<Purchase> queryPurchases() {
                Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases(BillingClient.SkuType.INAPP);
                return purchasesResult.getPurchasesList();
            }

            @Override
            public void onBillingServiceDisconnected() {
                //сюда мы попадем если что-то пойдет не так
                makeText(getApplicationContext(), "disconect", Toast.LENGTH_LONG);
                Log.d(TAG, "disconet");
            }
        });
    }


    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {
        if (purchases != null) {
            payComplete();
            Log.d(TAG, "onPurchasesUpdate() responseCode: "
                    + billingResult.getResponseCode());
            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && purchases != null) {
                processPurchases(purchases);
            } else {
                // Handle any other error codes.
                Log.d(TAG, "ERROR onPurchasesUpdated " + billingResult.toString());
            }
        }
    }

    private void payComplete() {
        Toast toast = makeText(this, "Вы приобрели товар!" +
                "ПОЗДРАВЛЯЕМ", Toast.LENGTH_LONG);
        toast.show();
        Log.d(TAG, "finished");


    }

    private void queryPurchasesAsync() {
        myPurchasesResultList.clear();
        Purchase.PurchasesResult purchasesResult =
                mBillingClient.queryPurchases(BillingClient.SkuType.INAPP);
        // If there are subscriptions supported, we add subscription rows as well

        Log.d(TAG, "Local Query Purchase List Size: "
                + purchasesResult.getPurchasesList().size());
        processPurchases(purchasesResult.getPurchasesList());
    }

    private void processPurchases(List<Purchase> purchases) {
        if (purchases.size() > 0) {
            Log.d(TAG, "purchase list size: " + purchases.size());
        }
        for (Purchase purchase : purchases) {
            if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
                handlePurchase(purchase);
            } else if (purchase.getPurchaseState() == Purchase.PurchaseState.PENDING) {
                Log.d(TAG, "Received a pending purchase of SKU: "
                        + purchase.getSku());
                // handle pending purchases, e.g. confirm with users about the pending
                // purchases, prompt them to complete it, etc.
                // TODO handle this in the next release.
            }
        }

        for (Purchase purchase : purchases) {
            if (purchase.getSku().equals(mSkuId1)) {
                handleConsumablePurchasesAsync(purchase);
            }
//            } else {
//                acknowledgeNonConsumablePurchasesAsync(purchase);
//            }
        }
    }


    private void handlePurchase(Purchase purchase) {
        Log.d(TAG, "Got a purchase: " + purchase);
        myPurchasesResultList.add(purchase);
    }

    private void handleConsumablePurchasesAsync(final Purchase purchase) {
        // If we've already scheduled to consume this token - no action is needed (this could happen
        // if you received the token when querying purchases inside onReceive() and later from
        // onActivityResult()
        if (tokensToBeConsumed == null) {
            tokensToBeConsumed = new HashSet<>();
        } else if (tokensToBeConsumed.contains(purchase.getPurchaseToken())) {
            Log.d(TAG,
                    "Token was already scheduled to be consumed - skipping...");
            return;
        }
        tokensToBeConsumed.add(purchase.getPurchaseToken());
        // Generating Consume Response listener
        final ConsumeResponseListener onConsumeListener = (billingResult, purchaseToken) -> {
            // If billing service was disconnected, we try to reconnect 1 time
            // (feel free to introduce your retry policy here).
            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                Log.d(TAG,
                        "onConsumeResponse, Purchase Token: " + purchaseToken);
            } else {
                Log.d(TAG, "onConsumeResponse: "
                        + billingResult.getDebugMessage());
            }
        };
        // Creating a runnable from the request to use it inside our connection retry policy below
//        Runnable consumeRequest = () -> {
        // Consume the purchase async
        ConsumeParams consumeParams = ConsumeParams.newBuilder()
                .setPurchaseToken(purchase.getPurchaseToken())
                .build();
        mBillingClient.consumeAsync(consumeParams, onConsumeListener);
//        };
//        executeServiceRequest(consumeRequest);
    }


    private void querySkuDetailsSubs() {
        SkuDetailsParams.Builder skuDetailsParamsBuilderSubs = SkuDetailsParams.newBuilder();
        List<String> skuListSubs = new ArrayList<>();
        skuListSubs.add(mSkuId1);
        skuDetailsParamsBuilderSubs.setSkusList(skuListSubs).setType(BillingClient.SkuType.INAPP);
        mBillingClient.querySkuDetailsAsync(skuDetailsParamsBuilderSubs.build(), new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> skuDetailsList) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    for (SkuDetails skuDetails : skuDetailsList) {
                        mSkuDetailsMap.put(skuDetails.getSku(), skuDetails);

                    }
                }
            }
        });
    }


    public void launchBilling(String mSkuId) {
        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(mSkuDetailsMap.get(mSkuId))
                .build();
        mBillingClient.launchBillingFlow(this, billingFlowParams);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adapter != null) {
            adapter.release();
            adapter = null;
        }
    }
}


