package ru.com.pershinanton.playbillinglibrary;

import android.content.Context;

import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseViewHolder<BIND_OBJECT> extends RecyclerView.ViewHolder implements BaseViewHolderInterface<BIND_OBJECT> {
    private final boolean useButterKnifeBinder;
    private Unbinder unbinder;
    private BIND_OBJECT bindObject;

    public BaseViewHolder(Context context, boolean useButterKnifeBinder, @LayoutRes int layoutResId) {
        super(Inflater.inflate(context, layoutResId));
        this.useButterKnifeBinder = useButterKnifeBinder;
        if (useButterKnifeBinder && unbinder == null)
            bindButterKnife();
    }

    public BaseViewHolder(Context context, @LayoutRes int layoutResId) {
        super(Inflater.inflate(context, layoutResId));
        this.useButterKnifeBinder = true;
        bindButterKnife();
    }

    public void onBind(int position, BIND_OBJECT object) {
        this.bindObject = object;
        if (useButterKnifeBinder && unbinder == null) bindButterKnife();
    }

    private void bindButterKnife() {
        if (unbinder == null)
            unbinder = ButterKnife.bind(this, this.itemView);
    }

    public void onViewRecycled() {
        if (this.unbinder != null) {
            this.unbinder.unbind();
            this.unbinder = null;
        }
        this.bindObject = null;
    }

    public BIND_OBJECT getBindObject() {
        return this.bindObject;
    }

    public void setBindObject(BIND_OBJECT bindObject) {
        this.bindObject = bindObject;
    }

}