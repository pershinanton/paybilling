package ru.com.pershinanton.playbillinglibrary;

public interface BaseViewHolderInterface<T> {
    void onBind(int position, T object);

    void onViewRecycled();
}
